#!/bin/bash

# This script copies Octoprint's init scripts to the proper location
# on a remote Ubuntu machine.

if [[ "$#" -ne 1 ]] || [[ $1 == -* ]]; then
	echo "usage: $0 <hostname>"
	exit 0
fi

cd "$( dirname "${BASH_SOURCE[0]}" )"
host="$1"
echo "copying files to ${host}."
scp clock-wait.conf octoprint.conf ubuntu@${host}:/tmp || exit 0
echo "installing files."
ssh -t ubuntu@${host} "sudo install -o root -m 644 /tmp/clock-wait.conf /tmp/octoprint.conf /etc/init/"
