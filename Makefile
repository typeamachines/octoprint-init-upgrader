files = README.md README.html deploy-init-scripts.bash cutpaste.txt	\
clock-wait.conf octoprint.conf

VERSION = $(shell git describe)

all: zip

clean:
	-rm deploy-init-scripts_$(VERSION).zip cutpaste.txt README.html

%.txt: %.txt.in
	m4 -P $< > $@

%.html : %.md
	markdown $< > $@

zip: deploy-init-scripts_$(VERSION).zip

deploy-init-scripts_$(VERSION).zip: deploy-init-scripts_$(VERSION)

deploy-init-scripts_$(VERSION): $(files)
	[ -d deploy-init-scripts_$(VERSION) ] || \
mkdir deploy-init-scripts_$(VERSION)
	cp $(files) deploy-init-scripts_$(VERSION)


%.zip: %
	zip -r  $@ $<
